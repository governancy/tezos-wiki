---
layout: post
title:  "Resources"
date:   2019-01-07 12:14:18
---
Resources
===========

# List of Tezos Groups {#foundation}

- [Tezos Foundation](https://tezos.foundation/)
- [Tezos Commons Foundation](https://tezoscommons.org/)
- [Tezos Japan](https://twitter.com/TezosJapan)
- [Tezos Korea](http://tezoskoreacommunity.org/)
- [Tezos Korea Foundation](http://tezoskorea.foundation/)
- [Tezos Southeast Asia](https://www.tezos.org.sg/)
- [Tezos Montreal](https://tezosmtl.com/)
- [Tezos OCaml Michelson Institute](https://tomi.institute/)
- [Tezos Detroit](https://twitter.com/TezosD)
- [Tezos Luxembourg](https://tezos.lu/)
- [Tezos Turkey](https://twitter.com/tezosturkey)
- [Tezos Geneva](https://twitter.com/TezosGeneva)

# Tezos Foundation Grant {#grant}

Apply [here](https://tezos.foundation/grants-overview) for a grant from the foundation to work on a project on the Tezos ecosystem.

# Wallets {#wallet}
- [Tezbox (Mobile)](https://tezbox.com/)
- [Cryptonomic Galleon](https://github.com/Cryptonomic/Deployments/wiki/Galleon:-Releases)
- [Cortez (Android)](https://play.google.com/store/apps/details?id=com.tezcore.cortez)
- [Kukai](https://kukai.app/)

# Block Explorer {#explorer}
- [Tzscan](https://tzscan.io/)
- [Tezex](https://tezex.info/)

# Baking Services {#baking}

A list of Bakers can be found [here](https://mytezosbaker.com/).